<footer class="">
    <svg id="curve" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1435 194">
        <path class="cls-1" d="M.5,139.5S421.69,345.53,661,236c260-119,501-75,774.5-49V333.5H.5Z" transform="translate(-0.5 -139.5)" /></svg>

    <div class="section-padding">

        <div class="container">
            <div class="col-lg-12">

                <div class="footer-title">
                    <h3 class="text-center">
                        <em>Affiliations</em>
                    </h3>
                </div>

                <div class="col-lg-10 col-md-11 mx-auto logos">
                    <div class="row">

                        <div class="col-md-2 col-6">
                            <img src="assets/img/image001 (5).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image002 (3).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image003 (3).png" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image004 (3).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image005 (3).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image006 (3).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/image007 (3).jpg" class="img-fluid">
                        </div>
                        
                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-1.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-2.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-3.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-4.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-5.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-6.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-7.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-8.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-9.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-10.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-11.png" class="img-fluid">
                        </div>

                        <div class="col-md-2 col-6">
                            <img src="assets/img/logos-12.png" class="img-fluid">
                        </div>

                    </div>
                </div>

                <!-- Socials -->
                <div class="social">
                    <ul>
                        <li><a href="https://www.facebook.com/JSKEtiquetteConsortium/" target="_blank" rel="noopener"><i class="fa fa-facebook circle-icon" aria-hidden="true" alt="Facebook"></i></a></li>
                        <li><a href="https://twitter.com/JSKEtiquette" target="_blank" rel="noopener"><i class="fa fa-twitter circle-icon" aria-hidden="true" alt="Twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/jsketiquette/" target="_blank" rel="noopener"><i class="fa fa-instagram circle-icon" aria-hidden="true" alt="Instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/pub/janet-adetu/35/93b/735" target="_blank" rel="noopener"><i class="fa fa-linkedin circle-icon" aria-hidden="true" alt="LinkedIn"></i></a></li>
                    </ul>
                </div>
                <!-- / Socials -->

                <!-- Address -->
                <div class="col-lg-10 col-md-11 mx-auto address">

                    <div class="float-lg-left">
                        <p>6B, Maitama Sule Street, Off Awolowo Road, SW Ikoyi, Lagos.</p>
                    </div>

                    <div class="float-lg-right">
                        <a class="mx-2" href="tel:0813183838">08131838380</a> <a class="mx-2" href="tel:08131838390">08131838390</a>
                    </div>

                    <div class="clearfix"></div>

                </div>
                <!-- /Address -->

                <!-- Quick Links -->
                <div class="col-lg-10 col-md-11 mx-auto footer-menu">
                    <ul id="menu-footer-menu" class="menu">
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/">Home</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/about-us/">About Us</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/meet-janet/">Meet Janet</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/services/">Professional Services</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/products/">Products</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/media/">Media</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/testimonials/">Testimonials</a></li>
                        <li class="d-md-inline"><a href="#">Blog</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/news-events/">News &amp; Events</a></li>
                        <li class="d-md-inline"><a href="https://jsketiquetteconsortium.com/contact-us/">Contact Us</a></li>
                    </ul>
                </div>
                <!-- /Quick Links -->

                <div class="col-lg-10 col-md-11 mx-auto footer-menu">
                    <div class="address">
                        <ul>
                            <li class="d-inline">Copyright © 2019 All Rights Reserved</li>|<li class="d-inline"><a href="https://jsketiquetteconsortium.com/privacy-policy/">Privacy Policy</a></li>|<li class="d-inline"><a href="https://jsketiquetteconsortium.com/sitemap">Sitemap</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<?php include 'scripts.php'; ?>