<!doctype html>
<html lang="en">

<head>
    <title>
        Graduate Leadership Program - JSK Etiquette Consortium
    </title>
    <!-- Charset -->
    <meta charset="utf-8">
    <!-- Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <!-- Meta Tags -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Graduate Leadership Program - JSK Etiquette Consortium" />
    <meta property="og:description" content="Extensive Skills Acquisition & Employability Training" />
    <meta property="og:site_name" content="Graduate Leadership Program - JSK Etiquette Consortium" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Extensive Skills Acquisition & Employability Training" />
    <meta name="twitter:title" content="Graduate Leadership Program - JSK Etiquette Consortium" />
    <meta name="description" content="Extensive Skills Acquisition & Employability Training">
    <meta name="keywords" content="jsk, register, skill">

    <!-- CSS -->
    <link rel="preconnect" href="https://stackpath.bootstrapcdn.com">
    <link rel="preconnect" href="https://cdnjs.cloudflare.com">
    <link rel="preconnect" href="https://code.jquery.com">
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/core.css">
    <link rel="stylesheet" href="assets/css/critical.css">
    
    <!-- WOW JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
    $(window).on('load', function() { 
        new WOW().init();
    });
    </script>
  
</head>