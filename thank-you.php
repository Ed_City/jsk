<?php include 'header.php'; ?>

<body class="animated fadeIn">

    <section class="page-hero" style="background-image: url('assets/img/hero-1.jpg');">
        <div class="container">
            <!-- Logo -->
            <div class="py-4">
                <img src="assets/img/logo.png" class="img-fluid logo">
            </div>
            <!-- / Logo -->

            <div class="row">
                <div class="col-lg-6 col-md-8 ml-auto align-self-center">
                    <h1 class="hero-text">
                        Your path to success has begun
                    </h1>
                </div>
            </div>
        </div>
        <svg id="curve" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1435 194">
            <path class="cls-1" d="M.5,139.5S421.69,345.53,661,236c260-119,501-75,774.5-49V333.5H.5Z" transform="translate(-0.5 -139.5)" /></svg>
    </section>

    <?php include 'footer.php'; ?>

    <script>
        $(function msg() {
            swal({
                icon: 'success',
                title: 'Thank You!',
                text: 'Your message has been sent.',
                closeOnClickOutside: false,
                closeOnEsc: false,
                buttons: false,
            });

            window.setTimeout(function() {
                // Move to a new location or you can do something else
                window.location.href = "index.php";
            }, 6000);
        });

        /*$(document).ready(function() {
            msg();
        });*/
    </script>
</body>

</html>