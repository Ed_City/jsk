<?php include 'header.php'; ?>

<body>

    <section class="page-hero" style="background-image: url('assets/img/hero-2.jpg');">
        <div class="container">
            <!-- Logo -->
            <div class="py-4 pseudo-nav">
                <div class="float-md-left">
                    <a href="https://jsketiquetteconsortium.com/">
                        <img src="assets/img/logo.png" class="img-fluid logo">
                    </a>
                </div>
                <div class="float-md-right">
                    <a href="register.php" class="btn btn-register">Click to Register</a>
                </div>

                <div class="clearfix"></div>
            </div>
            <!-- / Logo -->

            <div class="row">
                <div class="col-lg-6 col-md-8 ml-auto align-self-center">
                    <h1 class="hero-text">
                        Graduate
                        <br>
                        Leadership Program
                    </h1>
                    <p class="hero-desc">
                        Extensive Skills Acquisition &amp; Employability Training
                    </p>
                    
                    <p class="hero-desc">
                       <small>
                        Commencement Date: April 1st, 2019.
                        </small>
                    </p>
                </div>
            </div>
        </div>
        <svg id="curve" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1435 194">
            <path class="cls-1" d="M.5,139.5S421.69,345.53,661,236c260-119,501-75,774.5-49V333.5H.5Z" transform="translate(-0.5 -139.5)" /></svg>
    </section>

    <section class="container section-padding">
        <div class="col-lg-12">

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-1.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading teal">
                                Program Learning Outcomes
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        To provide essential leadership skills, that will propel participants to becoming intellectual leaders.
                                    </li>
                                    <li>
                                        To present business ethics and essential management skills applicable immediately in daily endeavors.
                                    </li>
                                    <li>
                                        To help participants determine their professional &amp; personal road map for success.
                                    </li>
                                    <li>
                                        To discover and adapt how technology strategies to upscale performance.
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-2.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading red">
                                For Whom:
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        Graduates
                                    </li>
                                    <li>
                                        New Intakes
                                    </li>
                                    <li>
                                        Junior Personnel
                                    </li>
                                    <li>
                                        Management Trainees
                                    </li>
                                    <span id="forWhom" class="collapse">
                                        <li>
                                            Emerging Professionals
                                        </li>
                                        <li>
                                            Young Entrepreneurs
                                        </li>
                                        <li>
                                            Individuals
                                        </li>
                                    </span>
                                </ul>
                                <ul class="url">
                                    <li class="link">
                                        <a data-toggle="collapse" href="#forWhom" role="button" aria-expanded="false" aria-controls="forWhom">Read More</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->



        </div>
    </section>

    <!-- Solo Banner -->
    <section class="container">
        <div class="col-lg-12">

            <div class="page-hero banner" style="background-image: url('assets/img/banner-1.png');">
                <div class="p-4">
                    <div class="row">
                        <div class="col-md-7 ml-auto align-self-center pr-lg-5">

                            <div class="heading yellow">
                                Duration
                            </div>

                            <div>
                                <p>
                                    The Graduate Leadership Program will run for a period of three months. The program includes two months of classroom training and a one-month job experience internship which provides an opportunity for participants to determine their personal and professional journey.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- / Solo Banner -->

    <section class="container section-padding">
        <div class="col-lg-12">

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-3.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading red">
                                Steps
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        Register
                                    </li>
                                    <li>
                                        Enrol
                                    </li>
                                    <li>
                                        Training/Workshops
                                    </li>
                                    <li>
                                        Case Study/Project
                                    </li>
                                    <span id="steps" class="collapse">
                                        <li>
                                            Internship
                                        </li>
                                    </span>
                                </ul>
                                <ul class="url">
                                    <li class="link">
                                        <a data-toggle="collapse" href="#steps" role="button" aria-expanded="false" aria-controls="steps">Read More</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-4.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading teal">
                                Faculty
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        Industry Experts
                                    </li>
                                    <li>
                                        Seasoned Certified Facilitators
                                    </li>
                                    <li>
                                        CEO Professionals
                                    </li>
                                    <li>
                                        UK Based Professional Project Management Mentor
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

        </div>
    </section>

    <!-- Solo Banner -->
    <section class="container">
        <div class="col-lg-12">

            <div class="page-hero banner" style="background-image: url('assets/img/banner-2.png');">
                <div class="p-4">
                    <div class="row">
                        <div class="col-md-7 ml-auto align-self-center pr-lg-5">

                            <div class="heading yellow">
                                Curriculum
                            </div>

                            <div>
                                <p>
                                    This program will cover core areas of business &amp; financial management, workplace productivity, leadership, entrepreneurial strategy and personal development. It is a complete detailed program to get you ready for excellent performance both personally and professionally. Companies will employ you when they see competence, self-confidence, integrity, and trustworthiness in you.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- / Solo Banner -->

    <section class="container section-padding pb-2">
        <div class="col-lg-12">

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-5.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading red">
                                Modules
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        Business Management
                                    </li>
                                    <li>
                                        Leadership Acquisition
                                    </li>
                                    <li>
                                        Entrepreneurship
                                    </li>
                                    <li>
                                        Financial Management
                                    </li>
                                    <span id="modules" class="collapse">
                                        <li>
                                            Impression Management
                                        </li>
                                        <li>
                                            Personal Brand Strategy
                                        </li>
                                        <li>
                                            Emotional Intelligence
                                        </li>
                                        <li>
                                            Employability Ready
                                        </li>
                                        <li>
                                            Connecting with Purpose
                                        </li>
                                        <li>
                                            Protocol for Business Relationships
                                        </li>
                                    </span>
                                </ul>
                                <ul class="url">
                                    <li class="link">
                                        <a data-toggle="collapse" href="#modules" role="button" aria-expanded="false" aria-controls="modules">Read More</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-6.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading teal">
                                Extra Benefits
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        One Year Membership of JSK Academy
                                    </li>
                                    <li>
                                        Media Production Experience
                                    </li>
                                    <li>
                                        Access to JSK Advisory Services
                                    </li>
                                    <li>
                                        Project Management Experience
                                    </li>
                                    <span id="extraBenefits" class="collapse">
                                        <li>
                                            CSR Initiative
                                        </li>
                                        <li>
                                            Job Retention Opportunity
                                        </li>
                                    </span>
                                </ul>
                                <ul class="url">
                                    <li class="link">
                                        <a data-toggle="collapse" href="#extraBenefits" role="button" aria-expanded="false" aria-controls="extraBenefits">Read More</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

            <!-- Text and Image Grid -->
            <div class="grid">
                <div class="row m-0">
                    <div class="col-lg-4 col-md-5 order-md-1">
                        <div>
                            <img src="assets/img/img-7.png" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 align-self-center">

                        <!-- Text Container -->
                        <div class="text-container">

                            <!-- Heading -->
                            <div class="heading teal">
                                Enrollment Fees
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        Course Fee - ₦350,000.00
                                    </li>
                                    <li>
                                        Early Bird Fee - ₦300,000.00
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                            <!-- Heading -->
                            <div class="heading yellow">
                                Payment Information
                            </div>
                            <!-- / Heading -->

                            <!-- Copy -->
                            <div class="copy">
                                <ul class="">
                                    <li>
                                        <strong>Cheque:</strong> JSK Etiquette International Ltd.
                                    </li>
                                    <li>
                                        <strong>Transfer:</strong> Access Bank Acct. 0706232705
                                    </li>
                                </ul>
                            </div>
                            <!-- / Copy -->

                        </div>
                        <!-- / Text Container -->
                    </div>
                </div>
            </div>
            <!-- / Text and Image Grid -->

        </div>
    </section>

    <section class="container">
        <div class="col-lg-12">
            <div>
                <a href="register.php" class="btn btn-register">Click to Register</a>
            </div>
        </div>
    </section>

    <?php include 'footer.php'; ?>
</body>

</html>