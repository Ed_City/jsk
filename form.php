<?php
require_once 'config.php';

// set timezone to user timezone
date_default_timezone_set("Africa/Lagos");
$currentDateTime = date('Y-m-d H:i:s');


if( isset( $_POST['submit'] ) )
{


function validateData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
    
    $title = validateData($_POST["title"]);
    $firstName = validateData($_POST["firstName"]);
    $lastName = validateData($_POST["lastName"]);
    $dob = validateData($_POST["dob"]);
    $age = validateData($_POST["age"]);
    $address = validateData($_POST["address"]);
    $phone = validateData($_POST["mobile"]);
  $email = validateData($_POST["email"]);
  $medium = validateData($_POST["medium"]);
  $occupation = validateData($_POST["occupation"]);
  $qualifications = validateData($_POST["qualifications"]);
  $career = validateData($_POST["career"]);
  $goals = validateData($_POST["goals"]);
    
    function render_email($firstName, $lastName) {
             ob_start();
             include "success.phtml";
             return ob_get_contents();
            }
    
    //Insert to database

    $sql = "INSERT INTO records (title, firstName, lastName, dob, ageBracket, address, mobileNo, email, medium, currentOccupation, qualifications, careerChoice, futureGoals, entryDate)
            VALUES ('$title', '$firstName', '$lastName', '$dob', '$age', '$address', '$phone', '$email', '$medium', '$occupation', '$qualifications', '$career', '$goals', '$currentDateTime')";

            if (mysqli_query($conn, $sql)) {
                
                    // Email to Client
$to = "olawale.abimbola@jsketiquetteconsortium.com";
$subject = "Graduate Leadership Program - JSK Etiquette Consortium";

$message = file_get_contents("email.html");

$header = "From:Campaigns @RDM<campaigns@ringier.ng> \r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-type: text/html\r\n";
    

$retval = mail ($to,$subject,$message,$header);
                
                // Email to Subscriber
$toS = $email;
$subjectS = "Submission Received - Graduate Leadership Program";

$messageS = render_email($firstName, $lastName);

$headerS = "From:Olawale from JSK<olawale.abimbola@jsketiquetteconsortium.com> \r\n";
$headerS .= "MIME-Version: 1.0\r\n";
$headerS .= "Content-type: text/html\r\n";
    

$retvalS = mail ($toS,$subjectS,$messageS,$headerS);

if( $retval ) {
	header('location:thank-you.php');
    //echo '<script type="text/javascript">window.location.replace("thank-you.php");</script>';
		exit();
} else {
    console.log("Email was not sent");
}

}else {
             $error = $sql . "<br>" . mysqli_error($conn);
                //console.log($error);
            }
    
}
?>


<?php include 'header.php'; ?>

<body class="animated fadeIn">

    <section class="page-hero" style="background-image: url('assets/img/hero-1.jpg');">
        <div class="container">
            <!-- Logo -->
            <div class="py-4">
                <img src="assets/img/logo.png" class="img-fluid logo">
            </div>
            <!-- / Logo -->

            <div class="row">
                <div class="col-lg-6 col-md-8 ml-auto align-self-center">
                    <h1 class="hero-text">
                        Try Again
                    </h1>
                </div>
            </div>
        </div>
        <svg id="curve" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1435 194">
            <path class="cls-1" d="M.5,139.5S421.69,345.53,661,236c260-119,501-75,774.5-49V333.5H.5Z" transform="translate(-0.5 -139.5)" /></svg>
    </section>

    <?php include 'footer.php'; ?>

    <script type="text/javascript">
        swal({
            icon: "error",
            title: "Error!",
            text: "Message not sent. You will be redirected back so you can try again.",
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: false
        });

        window.setTimeout(function() {

            // Move to a new location or you can do something else
            window.location.href = "index.php";

        }, 6000);
    </script>

</body>

</html>