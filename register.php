<?php include 'header.php'; ?>

<body>

    <section class="page-hero" style="background-image: url('assets/img/hero-1.jpg');">
        <div class="container">
            <!-- Logo -->
            <div class="py-4 pseudo-nav">
                <div class="float-md-left">
                    <a href="https://jsketiquetteconsortium.com/">
                        <img src="assets/img/logo.png" class="img-fluid logo">
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- / Logo -->

            <div class="row">
                <div class="col-lg-6 col-md-8 ml-auto align-self-center">
                    <h1 class="hero-text">
                        Register with us
                    </h1>
                </div>
            </div>
        </div>
        <svg id="curve" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1435 194">
            <path class="cls-1" d="M.5,139.5S421.69,345.53,661,236c260-119,501-75,774.5-49V333.5H.5Z" transform="translate(-0.5 -139.5)" /></svg>
    </section>

    <section class="container section-padding">
        <div class="col-lg-12">

            <div class="grid">
                <div class="p-3 px-5">

                    <div class="heading teal">
                        Register with us
                    </div>

                    <div>
                        <form action="form.php" method="post">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="title" class="form-control brand-filter" data-validation="required">
                                            <option disabled selected>Select Title</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Miss">Miss</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="firstName" class="form-control brand-filter" type="text" placeholder="First Name" onblur="this.placeholder='First Name'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="lastName" class="form-control brand-filter" type="text" placeholder="Last Name" onblur="this.placeholder='Last Name'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <p class="small">
                                        Date of Birth
                                    </p>
                                </div>
                                   <div class="col-md-4">
                                    <div class="form-group">
                                        <input name="dob" class="form-control brand-filter" type="text" placeholder="DD/MM/YYYY" onblur="this.placeholder='DD/MM/YYYY'" onfocus="this.placeholder=''" data-validation="date" data-validation-format="dd/mm/yyyy" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-4 offset-md-2">
                                    <div class="form-group">
                                        <select name="age" class="form-control brand-filter" data-validation="required">
                                            <option disabled selected>Age Bracket</option>
                                            <option value="18 - 30">18 - 30</option>
                                            <option value="31 - 45">31 - 45</option>
                                            <option value="46 - 60">46 - 60</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea name="address" class="form-control brand-filter" rows="3" placeholder="Address" onblur="this.placeholder='Address'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off"></textarea>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="mobile" class="form-control brand-filter" type="text" placeholder="Mobile" onblur="this.placeholder='Mobile'" onfocus="this.placeholder=''" data-validation="number" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="email" class="form-control brand-filter" type="email" placeholder="Email" onblur="this.placeholder='Email'" onfocus="this.placeholder=''" data-validation="email" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="small" for="medium">
                                           How did you hear about us?
                                       </label>
                                        <select name="medium" class="form-control brand-filter" data-validation="required" autocomplete="off">
                                            <option disabled selected>Please select an option</option>
                                            <option value="Affiliate">Affiliate</option>
                                            <option value="Email">Email</option>
                                            <option value="Employer">Employer</option>
                                            <option value="Event">Event</option>
                                            <option value="Friend">Friend</option>
                                            <option value="Google Search">Google Search</option>
                                            <option value="Leaflet">Leaflet</option>
                                            <option value="LinkedIn">LinkedIn</option>
                                            <option value="Newspaper/Magazine Article">Newspaper/Magazine Article</option>
                                            <option value="Past Participant">Past Participant</option>
                                            <option value="Poster">Poster</option>
                                            <option value="Social Media(Instagram/Twitter/Facebook)">Social Media(Instagram/Twitter/Facebook)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="occupation" class="form-control brand-filter" type="text" placeholder="Current Occupation" onblur="this.placeholder='Current Occupation'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="qualifications" class="form-control brand-filter" data-validation="required">
                                            <option disabled selected>Qualifications</option>
                                            <option value="WASSCE/SSCE">WASSCE/SSCE</option>
                                            <option value="OND">OND</option>
                                            <option value="HND">HND</option>
                                            <option value="Bachelors Degree">Bachelors Degree</option>
                                            <option value="Masters Degree">Masters Degree</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input name="career" class="form-control brand-filter" type="text" placeholder="Choice of Career" onblur="this.placeholder='Choice of Career'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <textarea name="goals" class="form-control brand-filter" rows="5" placeholder="Future Goals" onblur="this.placeholder='Future Goals'" onfocus="this.placeholder=''" data-validation="required" autocomplete="off"></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <button class="btn btn-register" type="submit" name="submit">
                                Submit
                            </button>
                            
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <?php include 'footer.php'; ?>
</body>
</html>